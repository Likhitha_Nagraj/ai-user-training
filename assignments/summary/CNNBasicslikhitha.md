CNN

->CNN is used for classification problem. Example: To classify a image as cat or dog.
->Classification is done for recognising the features.
-> Features are seperated using filters(ex:Filter for leg)
->The filter is made to slide image with particular stride to search particular feature.Afer recognising whether cat or dog we can use other filters for color, so on many filters for different features.

ARCHITECTURE
->Let us understand the architecture by an example . consider an image of number 2 as input .It is passed through first layer, where we create multiple filters with features of 2 ,and then for feature we get an o/p image called as feature map.
->Pooling is performed later- Taking the fixed max size grid and assigning pixel value 1. This procees is repeated until we get fine features.
->Activation fuction is used to decide whether to activate the neuron or not(ReLu).
->The feature maps are flattened to single vector which becomes the first layer of DNN.
->Last layer is the o/p layer (classifier) which classifies o/p and gives the probability for each class.
![image.png](./image.png)

NOTE
->Kernel size: How big should be the sliding window in kernels(usually smaller 1,3,5 or 7).
->Stride: How many pixels of kernel window should slide at each step of convolution.
->Zero padding: No of zeroes assigned to border of image so it completely filters each location including edges.
->No of filters: Controls number of patterns anf features convolution layer will look for.
->Flattening: Brings all feature maps to single vectotr 
