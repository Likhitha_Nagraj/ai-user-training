DNN

1)What is neural network
->Neural networks are inspired by brain,also called multilayer perceptron(feedforward neural network).
->Example consider an image with number 9 as an input- first layer: I/P layer, second layer- Edges, third layer- Pattern, Last layer- O/P layer.![image.png](./image.png)
-> In that each neuron holds a number known as activation.
-> Why layers- Because it tells us which combination of subcomponents corresponds to which digit.
-> We assign weights to each links ,where we calculate the sigmoid(1/1+e^x) of weighted sum of product of weights and activation of previous layer with bias.This equation gives us the activation of neurons in the next layer.
->Weights tell us what layers the neurons are picking and bias tells how high the weight should be before the starts being activated.
![image_1.png](./image_1.png)

2)Gradient descent 
->After getting an output value we dont how accurate the o/p is,so we calculate cost function(difference between expected o/p and target o/p).
->To calculate cost function(of previous example) we have
I/p - 13,002 weights and biases
o/p - The cost value
Prameters- Many training examples.
-> calculating 13,002 weights and biases and training it with many examples is atedious process hence we find local minima , where it gives the minimum cost function and tells how are expected o/p is approximate to target value (lower the cost function more accurate the output value).
-> The question is how to get this minima point , this is there we apply Gradient descent- Its an optimazation algorithm where we adjust parameters so we dont deviate much from the local minima.
-> Gradient descent is performed by taking steps proportional to the negative gradient vector.
![image_2.png](./image_2.png)

3) Backpropogation
->For example consider you need an o/p two neuron to be activated for i/p image of 2, and the activation inside it is 0.2 and the neuron 2 is most likely not to get activated unless its activation is almost near to one.
-> This can be done be either 1) Increasing the biasn 2) Increasing weights 3) Change activation of previous layers.
-> In order to get the result we form a list of nudges to happen in second and last layer and recursively apply same process to relevant weights and bias that determine the activation value in neuron 2, repeating same process and moving backwards.

4) Stochastic gradient descent
->Randomly arrange all numbers ,and construct batches of those numbers. Now run gradient descent for each batch.
This helps in getting local minima more efficiently.
